export default class Calculation {
    constructor(expression) {
        this.expression = expression;
        this.a = 0;
        this.b = 0;
    }

    addition() {
        return this.a + this.b;
    }

    subtraction() {
        return this.a - this.b;
    }

    production() {
        return this.a * this.b;
    }

    division() {
        return this.a / this.b;
    }

    calculate() {
        let pattern = /^([0-9]*\.?[0-9]*)(?:([-+*/])([0-9]*\.?[0-9]*)\s*)+$/;

        if (this.expression.match(pattern)) {
            let matches = pattern.exec(this.expression);

            let sign = matches[2];

            /**
             * @TODO Implement it
             */

            this.a = parseFloat(matches[1]);
            this.b = parseFloat(matches[3]);

            // console.log(JSON.stringify({matches: matches}));

            let result = false;

            switch (sign) {
                case '+':
                    result = this.addition();
                    break;
                case '-':
                    result = this.subtraction();
                    break;
                case '*':
                    result = this.production();
                    break;
                case '/':
                    result = this.division();
                    break;
            }

            return result;
        }
        else {
            console.log("Expression does not match: " + this.expression);
            return false;
        }
    }
}
